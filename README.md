
**O**ptimal **M**onomial **P**ure **Q**uadratization of **O**rdinary **D**ifferential **E**quations using **B**ranch and **B**ound with **A**dmissible **H**euristic

This Julia script is for pure quadratization of ordinary differential equations with multinomial right-hand-side functions. The van der Pol, quartic anharmonic oscillator and rabinovich-fabrikant ordinary differential equations are given as examples. 

Please check out https://doi.org/10.3390/math7040367 for details about the algorithm. In this implementation, there are only small modifications: the zero-effort checker is introduced, and tie-break of equal scores is performed differently. 
