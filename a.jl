#= 
 This Julia script is for optimal monomial pure quadratization of
 ordinary differential equations with multinomial right hand side
 functions. 
=#

using Combinatorics


#=
 This function takes a flattened vector that represents the ODE set 
 and the number of original unknowns as the input parameters. It 
 returns a single vector of vectors that represents all the possible 
 integer partitionings.
=#


function expandToVectorOfVectors(upToNow, A, N) 
  totalVec2 = Any[];

  tempAllLeftOfA = Any[];

  for counter = 1:2*N:size(A,1) 
    append!(tempAllLeftOfA, [A[counter:counter+N-1]]);
  end

  tempAllLeftOfA = unique(tempAllLeftOfA);


  upToNow = collect(Iterators.flatten(upToNow));

  tempAllLeftOfUpToNow = Any[];

  for counter=1:2*N:size(upToNow,1)
    append!(tempAllLeftOfUpToNow, [upToNow[counter:counter+N-1]]);
  end

  tempAllLeftOfUpToNow = unique(tempAllLeftOfUpToNow);

  for i = 1:2*N:size(A,1)
    totalVec = Any[];
    tempLeft = Any[];
    tempLeft = [A[i:i+N-1]];

    global tempRightCombined = A[i+N: i+2*N-1]; 


    z = [collect(multiexponents(2,xi)) for xi in tempRightCombined];

    z2 =  Iterators.product(z...);

    z3 = collect(z2);

    zeroEffort = false;

#= 
 Check if the term can be written without introducing new functions.
=# 

    for v in z3
      if(([first.(v)...] in [tempAllLeftOfA..., tempLeft, tempAllLeftOfUpToNow...]) && ([last.(v)...] in [tempAllLeftOfA..., tempLeft, tempAllLeftOfUpToNow...]) && (first.(v) <= last.(v)))
        push!(totalVec, collect(Iterators.flatten([first.(v),last.(v)]))); 
        zeroEffort = true;
        break;
      end
    end


#= 
 If the term cannot be written without introducing new functions,
 consider all possibilities.
=#

    if(zeroEffort == false)
      for v in z3        
        if (first.(v) <= last.(v))
          push!(totalVec, collect(Iterators.flatten([first.(v),last.(v)]))); 
        end   
      end
   end

    append!(totalVec2, [tempLeft, collect(totalVec)]);
   
  end

  return totalVec2;

end


#=
 This function finds out if a certain right hand side function
 appears on the left hand side.  
=#


function isExistent(myVec, oneTerm, N)
  if(oneTerm in findAllLeft(myVec, N))
    return true;
  else
    return false;      
  end
end


#=
 This function takes a vector of vectors and the number of
 original unknowns as input parameters. It returns a vector
 representing the first function that appears on the 
 right hand sides of the equation set but does not appear
 on the left hand side of the equation set. The function 
 returns an empty vector if all the right hand side functions
 also appear as left hand side functions. 
=#

function findFirstNonexistent(myVec, N)
  i = 2;
  sizeOfmyVec = size(myVec, 1);

  while (i <= sizeOfmyVec)
    oneRightSide = myVec[i];
    
    j = 1;
    
    while (j <= size(oneRightSide,1))

      oneTerm = oneRightSide[j:j+N-1];

      if(!(oneTerm in findAllLeft(myVec, N)))
        return oneTerm;   
      end

   
      j = j + N;
  
    end
    
    i = i + 2;

  end
  return [];
end



#= 
 This function takes a vector of vectors representing the ODE set and 
 the number of original unknowns as the input parameters. It returns 
 the distance traveled. The distance traveled is the number of 
 new unknown functions that have been introduced as left hand side
 functions.
=#


function distanceTraveled(myVec, N)
  return(size(findAllLeft(myVec, N),1)-N);
end



#=
 This function takes a vector of vectors representing the ODE set and 
 the number of original unknowns as the input parameters. It returns
 the minimum bound for the distance to the goal. The minimum bound
 for the distance to the goal is the number of unique unknown functions
 that appear on the right hand side but not on the left hand side.
=#


function minDistanceToGoal(myVec, N)
  i = 2;
  sizeOfmyVec = size(myVec, 1);

  allRightVec = Any[];

  while (i <= sizeOfmyVec)
    oneRightSide = myVec[i];

    j = 1;

    while (j <= size(oneRightSide,1))

      oneTerm = oneRightSide[j:j+N-1];

      if(!isExistent(myVec, oneTerm, N))
         push!(allRightVec, oneTerm)
      end

      j = j + N;

    end

    i = i + 2;

  end

  return size(unique(allRightVec),1);
end


#=
 This function calculates the score given the vector of vectors
 representing the ODE set and the number of original unknowns.
 The score is the distance traveled plus minimum bound for the
 distance to the goal.
=#


function calculateScore(myVec, N)
  return(distanceTraveled(myVec, N) +  minDistanceToGoal(myVec, N));
end



#=
 This function takes a vector of vectors (right hand sides combined)
 representing the ODE set and the number of original unknowns as the
 input parameters. It returns a vector of vectors that shows all the 
 unique left hand side multinomials.
=#


function findAllLeft(myVec, N)
  num = 1:2:size(myVec,1);
  return unique(myVec[num]);
end  


#= 
 This function takes the original flattened ODE, number of original
 unknowns and a single term. The function returns the vector that
 shows the single ODE that needs to be appended to the ODE set
 for a single space extension. 
=#

function makeNewEquation(A, N, oneTerm)
  tempVec = Any[];

  if ( oneTerm == zeros(Int64, size(oneTerm, 1)) )
    tempVec = append!(oneTerm, zeros(Int64, N));
  else
    myI = 1;
    tempVec2 = Any[];
    while (myI < size(A,1))
      tempLeft = Any[];
      tempLeft = A[myI:myI+N-1];
      tempRight = A[myI+N:myI+2*N-1];
      oneInd = findmax(tempLeft)[2];
      tempVec2 = oneTerm + tempRight;
      tempVec2[oneInd] -= 1;
      if(tempVec2[oneInd]>=0)
        oneTerm2 = copy(oneTerm);
        append!(oneTerm2, tempVec2);
        append!(tempVec, oneTerm2);
      end
      myI += 2*N;
    end          
  end
  return tempVec;
end



function pureQuadratize(totalVec, A, N)
  print("\n");

  global y = (collect(Base.product(totalVec...)));
 
  y = [y...];

  global stepInd = 2;

  global doneIt = false;

  print("\n\n");


  while (doneIt == false)
    print("\nStep ", stepInd, "\n");
    global stepInd += 1;

    global indexOfTheOneToExtend = 1;
    global smallestScore = Inf;
    global minDistanceToGoalOfSmallestScore = Inf;


    for counter = 1:size(y,1)
      myValue = y[counter];
      if (calculateScore(myValue,N) <= smallestScore)
        global smallestScore = calculateScore(myValue,N);
        global minDistanceToGoalOfSmallestScore = minDistanceToGoal(myValue,N);
        indexOfTheOneToExtend = counter;
      end
    end

   if (minDistanceToGoalOfSmallestScore == 0)
     print("We have a hit. It is given below:\n");
     print(y[indexOfTheOneToExtend]);
     print("\nDistance traveled: ");
     print(distanceTraveled(y[indexOfTheOneToExtend],N));
     print("\nMinimum distance to goal: ");
     print(minDistanceToGoal(y[indexOfTheOneToExtend],N));
     print("\nScore: ");
     print(calculateScore(y[indexOfTheOneToExtend],N));
     print("\n");

     print("\n");
     global doneIt = true;
     break;
   end



   i = y[indexOfTheOneToExtend];

   if (doneIt == true)
     break;
   end


   myC = findFirstNonexistent(i,N);

   d = makeNewEquation( A, N, myC ) ;

   print("\n");
   print("The new equation is\n");
   print( d );
   print("\n");


   q = expandToVectorOfVectors(i, d , N);
   print("Expanding it to vector of vectors gives\n");
   print(q);
   print("\n");
   

   q2 = collect(Base.product(q...));

   deleteat!(y, indexOfTheOneToExtend);


   for myValue in q2
     y = [y...,[i...,myValue...]];  
   end

   for myValue in y
     print(myValue);
     print("\n");
     print("Minimum distance to goal: ");
     print(minDistanceToGoal(myValue,N));
     print("\nScore: ");
     print(calculateScore(myValue,N));
     print("\n");
   end
  print("\n\n\n");

  end

end




#= 
 The equation below is the van der Pol ODE. 
 It is given in LaTeX below.
 \begin{eqnarray}
  \dot{u}^{(1,0)} &=& 1\, u^{(0,0)} u^{(1,0)} \nonumber \\
 &-& 1/3\, u^{(1,0)} u^{(2,0)} \nonumber \\
 &-& 1\, u^{(0,0)} u^{(0,1)}\\
  \dot{u}^{(0,1)} &=& 1\, u^{(0,0)} u^{(1,0)}
 \end{eqnarray}

 The ODE after optimal monomial pure quadratization is 
 expected to be in the form below.
 \begin{eqnarray}
  \dot{u}^{(1,0)} &=& 1\, u^{(0,0)} u^{(1,0)} \nonumber \\
 &-& 1/3\, u^{(1,0)} u^{(2,0)} \nonumber \\
 &-& 1\, u^{(0,0)} u^{(0,1)}\\
  \dot{u}^{(0,1)} &=& 1\, u^{(0,0)} u^{(1,0)}\\
  \dot{u}^{(0,0)} &=& 0\\
  \dot{u}^{(2,0)} &=& 2\, u^{(1,0)} u^{(1,0)} \nonumber \\
 &-& 2/3\, u^{(2,0)} u^{(2,0)} \nonumber \\
 &-& 2\, u^{(0,1)} u^{(1,0)}
 \end{eqnarray}
=# 

A = [  1, 0,  1, 0,
       1, 0,  3, 0,
       1, 0,  0, 1,
       0, 1,  1, 0];


N = 2;


totalVec = Any[];

totalVec = expandToVectorOfVectors([],A, N);

print("van der Pol:\n");
print("TotalVec: ");
print(totalVec);

pureQuadratize(totalVec, A, N)



#= 
 The equation below is the classical quartic anharmonic oscillator. 
 It is given in LaTeX below.
 \begin{eqnarray}
  \dot{u}^{(1,0)} &=& 1\, u^{(0,0)} u^{(0,1)}\\
  \dot{u}^{(0,1)} &=& -1\, u^{(0,0)} u^{(1,0)} \nonumber \\
 &-& 1\, u^{(1,0)} u^{(2,0)}
 \end{eqnarray}

 The ODE after optimal monomial pure quadratization is 
 expected to be in the form below.
 \begin{eqnarray}
  \dot{u}^{(1,0)} &=& 1\, u^{(0,0)} u^{(0,1)}\\
  \dot{u}^{(0,1)} &=& -1\, u^{(0,0)} u^{(1,0)} \nonumber \\
 &-& 1\, u^{(1,0)} u^{(2,0)}\\
  \dot{u}^{(0,0)} &=& 0\\
  \dot{u}^{(2,0)} &=& 2\, u^{(0,1)} u^{(1,0)}
 \end{eqnarray}
=#


A = [  1, 0,  0, 1,
       0, 1,  1, 0,
       0, 1,  3, 0 ];



N = 2;


totalVec = Any[];

totalVec = expandToVectorOfVectors([],A, N);


print("quartic anharmonic oscillator:\n");
print("TotalVec: ");
print(totalVec);


pureQuadratize(totalVec, A, N)


#= 
 The equation below is the Henon-Heiles. 
 It is given in LaTeX below.
\begin{eqnarray}
  \dot{u}^{(1,0,0,0)} &=& 1\, u^{(0,0,0,0)} u^{(0,1,0,0)}\\
  \dot{u}^{(0,1,0,0)} &=& -1\, u^{(0,0,0,0)} u^{(1,0,0,0)} \nonumber \\
 &-& 2\, u^{(0,0,1,0)} u^{(1,0,0,0)}\\
  \dot{u}^{(0,0,1,0)} &=& 1\, u^{(0,0,0,0)} u^{(0,0,0,1)}\\
  \dot{u}^{(0,0,0,1)} &=& -1\, u^{(0,0,0,0)} u^{(0,0,1,0)} \nonumber \\
 &-& 1\, u^{(1,0,0,0)} u^{(1,0,0,0)} \nonumber \\
 &+& 1\, u^{(0,0,1,0)} u^{(0,0,1,0)}
\end{eqnarray}

 The ODE after optimal monomial pure quadratization is 
 expected to be in the form below.
\begin{eqnarray}
  \dot{u}^{(1,0,0,0)} &=& 1\, u^{(0,0,0,0)} u^{(0,1,0,0)}\\
  \dot{u}^{(0,1,0,0)} &=& -1\, u^{(0,0,0,0)} u^{(1,0,0,0)} \nonumber \\
 &-& 2\, u^{(0,0,1,0)} u^{(1,0,0,0)}\\
  \dot{u}^{(0,0,1,0)} &=& 1\, u^{(0,0,0,0)} u^{(0,0,0,1)}\\
  \dot{u}^{(0,0,0,1)} &=& -1\, u^{(0,0,0,0)} u^{(0,0,1,0)} \nonumber \\
 &-& 1\, u^{(1,0,0,0)} u^{(1,0,0,0)} \nonumber \\
 &+& 1\, u^{(0,0,1,0)} u^{(0,0,1,0)}\\
  \dot{u}^{(0,0,0,0)} &=& 0
\end{eqnarray}
=#


A = [
    1, 0, 0, 0,  0, 1, 0, 0,
    0, 1, 0, 0,  1, 0, 0, 0,
    0, 1, 0, 0,  1, 0, 1, 0, 
    0, 0, 1, 0,  0, 0, 0, 1,
    0, 0, 0, 1,  0, 0, 1, 0,
    0, 0, 0, 1,  2, 0, 0, 0,
    0, 0, 0, 1,  0, 0, 2, 0 ];

N = 4;


totalVec = Any[];

totalVec = expandToVectorOfVectors([],A, N);


print("Henon-Heiles:\n");
print("TotalVec: ");
print(totalVec);


pureQuadratize(totalVec, A, N)


#= 
 The equation below is the Rabinovich-Fabrikant. 
 It is given in LaTeX below.
 \begin{eqnarray}
  \dot{u}^{(1,0,0)} &=& 1\, u^{(0,0,1)} u^{(0,1,0)} \nonumber \\
 &-& 1\, u^{(0,0,0)} u^{(0,1,0)} \nonumber \\
 &+& 1\, u^{(1,0,0)} u^{(1,1,0)} \nonumber \\
 &+& 1\, u^{(0,0,0)} u^{(1,0,0)}\\
  \dot{u}^{(0,1,0)} &=& 3\, u^{(0,0,1)} u^{(1,0,0)} \nonumber \\
 &+& 1\, u^{(0,0,0)} u^{(1,0,0)} \nonumber \\
 &-& 1\, u^{(1,0,0)} u^{(2,0,0)} \nonumber \\
 &+& 1\, u^{(0,0,0)} u^{(0,1,0)}\\
  \dot{u}^{(0,0,1)} &=& -2\, u^{(0,0,0)} u^{(0,0,1)} \nonumber \\
 &-& 2\, u^{(0,1,0)} u^{(1,0,1)}
 \end{equation}
=#

A =[
    1, 0, 0,  0, 1, 1,
    1, 0, 0,  0, 1, 0,
    1, 0, 0,  2, 1, 0,
    1, 0, 0,  1, 0, 0,
    0, 1, 0,  1, 0, 1,
    0, 1, 0,  1, 0, 0,
    0, 1, 0,  3, 0, 0,
    0, 1, 0,  0, 1, 0,
    0, 0, 1,  0, 0, 1,
    0, 0, 1,  1, 1, 1];

N = 3;

totalVec = Any[];

totalVec = expandToVectorOfVectors([],A, N);


print("rabinovich-fabrikant:\n");
print("TotalVec: ");
print(totalVec);


pureQuadratize(totalVec, A, N)

print("\nGoodbye.\n");

